{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards   #-}
module BatchTradeStreamingExample
    (
    batchTradeStreaming
    ) where
import qualified Data.ByteString.Lazy                      as BL
import           Data.Function                              (fix)
import qualified Data.Text                                 as T
import qualified Data.Text.Lazy.Builder                    as B
import qualified Data.Text.Lazy.Builder.Int                as B
import           Data.Word
import           GHC.Generics                              (Generic)
import           Network.GRPC.LowLevel
import           Network.GRPC.HighLevel.Generated
import           Proto3.Suite.Class
import           TradeService
import           ProtoTrade

clientConfig :: ClientConfig
clientConfig = ClientConfig { clientServerHost = "localhost"
                            , clientServerPort = 9000
                            , clientArgs = []
                            , clientSSLConfig = Nothing
                            , clientAuthority = Nothing
                            }

batchTradeStreaming :: IO ()
batchTradeStreaming = withGRPCClient clientConfig $ \client -> do
  TradeService{..} <- tradeServiceClient client

  doGetTradesBatch client 10000

  return()

getTradesBatch :: MethodName
getTradesBatch = MethodName "/com.benchmark.grpc.TradeService/getTradesBatch"

doGetTradesBatch :: Client -> Int -> IO ()
doGetTradesBatch c n = do
  let batchSize = 1000
  let expectedBatches = n `div` 1000

  rm <- clientRegisterMethodServerStreaming c getTradesBatch
  let pay        = QueryRequest (fromIntegral n) --using int instead of uint in example proto
      enc        = BL.toStrict . toLazyByteString $ pay
      err desc e = fail $ "doGetTradesBatch: " ++ desc ++ " error: " ++ show e
  eea <- clientReader c rm n enc mempty $ \_cc _md recv -> do
    n2 <- flip fix (0::Int) $
      \go i -> recv >>=
      \case
      Left e          -> err "recv" e
      Right Nothing   -> return i
      Right (Just bs) -> case fromByteString bs of
        Left e  -> err "decoding" e
        Right (TradeList tradeList)
            -> expect "getTradesBatch/rpy" batchSize (length(tradeList))  >> go (i+1)
    expect "getTrades/cnt" expectedBatches n2
  case eea of
    Left e             -> err "clientReader" e
    Right (_, st, _)
      | st /= StatusOk -> fail "clientReader: non-OK status"
      | otherwise      -> putStrLn "getTrades: RPC successful"
  where
    expay     = "not used"

expect :: (Eq a, MonadFail m, Show a) => String -> a -> a -> m ()
expect ctx ex got
  | ex /= got = fail $ ctx ++ " error: expected " ++ show ex ++ ", got " ++ show got
  | otherwise = return ()