{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards   #-}
module TradeStreamingExample
    (
    tradeStreaming
    ) where
import qualified Data.ByteString.Lazy                      as BL
import           Data.Function                              (fix)
import qualified Data.Text                                 as T
import qualified Data.Text.Lazy.Builder                    as B
import qualified Data.Text.Lazy.Builder.Int                as B
import           Data.Word
import           GHC.Generics                              (Generic)
import           Network.GRPC.LowLevel
import           Network.GRPC.HighLevel.Generated
import           Proto3.Suite.Class
import           TradeService
import           ProtoTrade

clientConfig :: ClientConfig
clientConfig = ClientConfig { clientServerHost = "localhost"
                            , clientServerPort = 9000
                            , clientArgs = []
                            , clientSSLConfig = Nothing
                            , clientAuthority = Nothing
                            }

tradeStreaming :: IO ()
tradeStreaming = withGRPCClient clientConfig $ \client -> do
  TradeService{..} <- tradeServiceClient client

  doGetTrades client 10

  return()

getTrades :: MethodName
getTrades = MethodName "/com.benchmark.grpc.TradeService/getTrades"

doGetTrades :: Client -> Int -> IO ()
doGetTrades c n = do
  rm <- clientRegisterMethodServerStreaming c getTrades
  let pay        = QueryRequest (fromIntegral n) --using int instead of uint in example proto
      enc        = BL.toStrict . toLazyByteString $ pay
      err desc e = fail $ "doGetTrades: " ++ desc ++ " error: " ++ show e
  eea <- clientReader c rm n enc mempty $ \_cc _md recv -> do
    n' <- flip fix (0::Int) $ \go i -> recv >>= \case
      Left e          -> err "recv" e
      Right Nothing   -> return i
      Right (Just bs) -> case fromByteString bs of
        Left e  -> err "decoding" e
        Right (ProtoTrade tradeId _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
            -> expect "getTrades/rpy" (intToText i) (tradeId)  >> go (i+1)
    expect "getTrades/cnt" n n'
  case eea of
    Left e             -> err "clientReader" e
    Right (_, st, _)
      | st /= StatusOk -> fail "clientReader: non-OK status"
      | otherwise      -> putStrLn "getTrades: RPC successful"
  where
    expay     = "not used"

intToText = B.toLazyText . B.decimal

expect :: (Eq a, MonadFail m, Show a) => String -> a -> a -> m ()
expect ctx ex got
  | ex /= got = fail $ ctx ++ " error: expected " ++ show ex ++ ", got " ++ show got
  | otherwise = return ()