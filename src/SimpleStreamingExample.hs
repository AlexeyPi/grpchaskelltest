{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards   #-}
module SimpleStreamingExample
    (
    simpleStreaming
    ) where
import qualified Data.ByteString.Lazy                      as BL
import           Data.Function                              (fix)
import qualified Data.Text                                 as T
import           Data.Word
import           GHC.Generics                              (Generic)
import           Network.GRPC.LowLevel
import           Network.GRPC.HighLevel.Generated
import           Proto3.Suite.Class
import           TradeService

--Generated from GRPC and copied here
data SSRqt = SSRqt { ssName :: T.Text, ssNumReplies :: Word32 } deriving (Show, Eq, Ord, Generic)
instance Message SSRqt
data SSRpy = SSRpy { ssGreeting :: T.Text } deriving (Show, Eq, Ord, Generic)
instance Message SSRpy

clientConfig :: ClientConfig
clientConfig = ClientConfig { clientServerHost = "localhost"
                            , clientServerPort = 9000
                            , clientArgs = []
                            , clientSSLConfig = Nothing
                            , clientAuthority = Nothing
                            }

simpleStreaming :: IO ()
simpleStreaming = withGRPCClient clientConfig $ \client -> do
  TradeService{..} <- tradeServiceClient client

  doHelloSS client 10

  return()

helloSS :: MethodName
helloSS = MethodName "/com.benchmark.grpc.TradeService/HelloSS"

doHelloSS :: Client -> Int -> IO ()
doHelloSS c n = do
  rm <- clientRegisterMethodServerStreaming c helloSS
  let pay        = SSRqt "server streaming mode" (fromIntegral n)
      enc        = BL.toStrict . toLazyByteString $ pay
      err desc e = fail $ "doHelloSS: " ++ desc ++ " error: " ++ show e
  eea <- clientReader c rm n enc mempty $ \_cc _md recv -> do
    n' <- flip fix (0::Int) $ \go i -> recv >>= \case
      Left e          -> err "recv" e
      Right Nothing   -> return i
      Right (Just bs) -> case fromByteString bs of
        Left e  -> err "decoding" e
        Right r -> expect "doHelloSS/rpy" expay (ssGreeting r) >> go (i+1)
    expect "doHelloSS/cnt" n n'
  case eea of
    Left e             -> err "clientReader" e
    Right (_, st, _)
      | st /= StatusOk -> fail "clientReader: non-OK status"
      | otherwise      -> putStrLn "doHelloSS: RPC successful"
  where
    expay     = "Hello there, server streaming mode!"

expect :: (Eq a, MonadFail m, Show a) => String -> a -> a -> m ()
expect ctx ex got
  | ex /= got = fail $ ctx ++ " error: expected " ++ show ex ++ ", got " ++ show got
  | otherwise = return ()