{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedLists   #-}

module PingPongExample
    (
    pingPong
    ) where
import           Network.GRPC.HighLevel.Generated
import           TradeService

clientConfig :: ClientConfig
clientConfig = ClientConfig { clientServerHost = "localhost"
                            , clientServerPort = 9000
                            , clientArgs = []
                            , clientSSLConfig = Nothing
                            , clientAuthority = Nothing
                            }

pingPong :: IO ()
pingPong = withGRPCClient clientConfig $ \client -> do
  TradeService{..} <- tradeServiceClient client

  ClientNormalResponse (PongResponse x) _meta1 _meta2 _status _details
    <- tradeServicepingPong (ClientNormalRequest (PingRequest 42) 1 [])
  putStrLn ("Pong response is = " ++ show x)

  return ()