module Main where

import SimpleStreamingExample   (simpleStreaming)
import TradeStreamingExample    (tradeStreaming)
import BatchTradeStreamingExample    (batchTradeStreaming)
import PingPongExample          (pingPong)

main :: IO ()
main = batchTradeStreaming
--main = tradeStreaming
--main = simpleStreaming
--main = pingPong
