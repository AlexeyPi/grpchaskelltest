# grpcHaskellTest
Test grpc client that connects to Java grpc server.

next communication patterns are implemented:
- pingPong - request-response;

- simpleStreaming - server sends stream of strings;

- tradeStreaming - server sends stream of multi-field objects;

- batchTradeStreaming - server sends stream of lists of multi-field objects (grouping into sub-batches/list significantly improves performance);


NOTES for building Mac OS on ARM M1 chip:
0. export PATH="/opt/homebrew/opt/llvm@12/bin:$PAT
1. Run 
'''stack build --exec grpcHaskellTest-exe  --extra-lib-dirs=/opt/homebrew/Cellar/grpc/1.41.1/lib --extra-include-dirs=/opt/homebrew/Cellar/grpc/1.41.1/include'''